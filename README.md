# Elixir course

This will be a gentle introduction to Elixir. It assumes no previous experience with the language, however I _will_ assume that you have a reasonable level of experience with at least one other programming language and basic programming concepts.

The first few workshops will focus on gently introducing you to the language through simple excercises inspired largely by [Advent of Code](https://adventofcode.com/). By the end of each workshop, you will have the skills and knowledge to tackle one or more Advent of Code challenges in Elixir. We focus specifically on skills to solve the first few challenges in [Advent of Code 2017 edition](https://adventofcode.com/2017)

## What you will learn

By the end of these introductory workshops you will be familiar with:

- basic Elixir syntax
- the difference between .ex and .exs files
- compiling and running Elixir code
- using `iex` for interactively developing in Elixir
- basic data types in Elixir such as atoms, lists and maps
- how Elixir code is structured using modules and functions
- approaching solutions as a series of data transformations
- how pattern matching works and why it is so powerful
- 'functional' approaches to working with lists

## References from slides

- BEAM's insane availability:

  https://en.wikipedia.org/wiki/High_availability#"Nines"

### Real-world users

- bet365

  https://sbcnews.co.uk/features/2017/09/26/erlang-elixir-programming-language/

- Netflix

  https://www.youtube.com/watch?v=_ploxRVSlN0

- Bleacher Report

  https://www.techworld.com/apps-wearables/how-elixir-helped-bleacher-report-handle-8x-more-traffic-3653957/

- Farmbot

  https://farm.bot/

- Discord

  https://blog.discordapp.com/scaling-elixir-f9b8e1e7c29b?gi=6c78ee0bdfd6

## Learn more

### Formal documentation

- Elixir official website: https://elixir-lang.org
- Elixir docs: https://elixir-lang.org/docs.html
- Phoenix official website: http://www.phoenixframework.org
- Phoenix guides: http://phoenixframework.org/docs/overview
- Phoenix docs: https://hexdocs.pm/phoenix
- Ecto docs: https://hexdocs.pm/ecto

### Exercises, examples

- Elixir examples: https://elixir-examples.github.io
- 30 Days of Elixir: https://github.com/seven1m/30-days-of-elixir
- exercism: https://exercism.io/tracks/elixir
- Hackerrank: https://hackerrank.com

### Tutorials, podcasts etc

- Alchemist Camp: https://alchemist.camp/episodes
  - [Screencast on setting up your editing environment](https://alchemist.camp/episodes/configuring-editor-plugins-and-formatter)
- ElixirCasts: https://elixircasts.io
- Daily Drip ($): https://www.dailydrip.com/topics/elixir
- LearnElixir.tv ($): https://www.learnelixir.tv
- LearnPhoenix.tv ($): https://www.learnphoenix.tv
- Elixir for Programmers ($): https://codestool.coding-gnome.com/courses/elixir-for-programmers
- Pluralsight "Meet Elixir" ($): https://www.pluralsight.com/courses/meet-elixir

### Conference videos

- Confreaks.tv: http://confreaks.tv/tags/40
  - [Elixir Conf EU](http://confreaks.tv/conferences/elixir-conf-eu)
  - [Elixir Conf](http://confreaks.tv/conferences/elixir-conf)
- ErlangSolutions Youtube channel: https://www.youtube.com/user/ErlangSolutions/
  - [ElixirConf EU 2018](https://www.youtube.com/watch?v=KGNfqiVF234&list=PLWbHc_FXPo2hJchaMDq_5FGn-lmGf-DAn)
  - [ElixirConf EU 2017](https://www.youtube.com/watch?v=kMHXd_iMGRU&list=PLWbHc_FXPo2jV6N5XEjbUQe2GkYcRkZdD)
  - [Elixir.LDN 2017](https://www.youtube.com/watch?v=zqzkrUVfv-k&list=PLWbHc_FXPo2h8_H-hZKYjNLTl3UraSyQ7)
  - [Lambda Days 2018](https://www.youtube.com/watch?v=RCU5WQDT8_8&list=PLWbHc_FXPo2jaxwnNB7KFEV7HYA0qHVxl)

### Jobs

- Plataformatec jobs board (official): http://plataformatec.com.br/elixir-radar/jobs
- Elixir Forum jobs board: https://elixirforum.com/c/elixir-jobs
- ElixirDose jobs board: https://elixir.career
- Elixir Jobs: https://elixirjobs.net/
- Stackoverflow jobs: https://stackoverflow.com/jobs/developer-jobs-using-elixir

### News

- ElixirStatus: http://elixirstatus.com/
- ElixirWeekly newsletter: https://elixirweekly.net/
- Elixir Radar (official): http://plataformatec.com.br/elixir-radar
- Elixir Digest: https://elixirdigest.net/

### Community

- Elixir Slack: https://elixir-slackin.herokuapp.com/
- Elixir Forum: https://elixirforum.com/

### Interesting companies

- Cultivate: https://cultivatehq.com/posts/
  _Major sponsor of many Elixir conferences and community meet-ups; occasional Elixir content on their blog_
- ThoughtBot: https://thoughtbot.com/services/elixir-phoenix
  _Creator of some of the more prominent Elixir libraries, as well as a blog and podcast featuring occasional Elixir content_

### Misc resources

- Elixir style guide: https://github.com/christopheradams/elixir_style_guide
- Elixir Fiddle: https://elixirfiddle.com/
- Elixir flash cards: https://elixircards.co.uk/
- awesome elixir: https://github.com/h4cc/awesome-elixir
- Elixir cheat sheet: http://media.pragprog.com/titles/elixir/ElixirCheat.pdf
