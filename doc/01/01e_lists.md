# Lists

- Lists

  ```
  [1,2,3,4,5,6,7,8,9]

  ["bananas", "paper towels", "AA batteries", "light bulbs"]

  [
      {35, :jack_daniels},
      {15, :triple_sec},
      {8, :lemon_juice},
      {250, :lemonade},
      :cubed_ice,
      :lemon_slices
  ]
  ```

- Adding and removing items from lists

  `[1, 2, 3, 4, 5, 6, 7, 8, 9] -- [4, 6, 1]`

  ```
  [2, 3, 5, 7, 8, 9]
  ```

  `[1, 2, 3] ++ [4, 6, 1]`

  ```
  [1, 2, 3, 4, 6, 1]
  ```

  `[1, :foo, 2, :bar:, 3, :foo, 4, :bar, 5] -- [:foo, :bar, :spam]`

  ```
  [1, 2, 3, :foo, 4, :bar, 5]
  ```

- Getting length of a list

  `length[4, 6, 1]`

  ```
  3
  ```

- Getting the front ("head") and remainder ("tail") of a list

  `hd([1, 2, 3, 4, 5])`

  ```
  1
  ```

  `tl([1, 2, 3, 4, 5])`

  ```
  [2, 3, 4, 5]
  ```

# Strings

- Working with strings as lists:

  `String.graphemes("Hello")`

  ```
  ["H", "e", "l", "l", "o"]
  ```

  `String.split("Hello world!", " ")`

  ```
  ["Hello", "world!"]
  ```

- String interpolation

  `name = "Phil"; IO.puts("The best Collins is #{name} Collins!")`

  ```
  "The best Collins is Phil Collins!"
  ```

# Enum

- Invoke a function for each item in an enumerable

  `Enum.each([1, 2, 3], fn(value) -> IO.puts("Hurrah!") end)`

  ```
  "Hurrah!"
  "Hurrah!"
  "Hurrah!"
  ```

- Apply a transformation to each item in an enumerable

  `Enum.map([1, 2, 3], fn(value) -> value * 3 end)`

  ```
  [3, 6, 9]
  ```

- Join two enumerables

  `Enum.concat(["H", "e"], ["l", "l", "o"])`

  ```
  ["H", "e", "l", "l", "o"]
  ```

- Return only the items in an enumerable which return a truthy value when mapped

  `Enum.filter([1, 2, 3, 4, 5], fn(value) -> rem(value, 2) == 0 end)`

  ```
  [2, 4]
  ```

- Reverse an enumerable

  `Enum.reverse([1, 2, 3, 4, 5])`

  ```
  [5, 4, 3, 2, 1]
  ```

## Further reading

https://hexdocs.pm/elixir/List.html

https://hexdocs.pm/elixir/String.html

https://hexdocs.pm/elixir/Enum.html
