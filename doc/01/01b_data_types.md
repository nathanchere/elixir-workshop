# Data types

- Integers

  ```
  5

  -100

  1012019301934130

  910394

  1_000_000_000

  0b10101

  0x8008135
  ```

- Floats

  ```
  1.0

  3.14159265359

  7.15e-8
  ```

- Booleans

  ```
  true

  false

  :true

  :false
  ```

- Strings

  ```
  "Hello, world!"

  "Heja Eslöv!"

  """
  How can you just walk away from me
  When all I can do is watch you leave?
  'Cause we've shared the laughter and the pain
  And even shared the tears
  You're the only one who really knew me at all
  """
  ```

- Tuples

  ```
  {:ok, "S3CR3TP4SSW0RD"}
  {"Invisible Touch", 1986, "Atlantic Records"}
  ```

- Lists

  ```
  [1,2,3,4,5,6,7,8,9]

  ["bananas", "paper towels", "AA batteries", "light bulbs"]

  [
      {35, :jack_daniels},
      {15, :triple_sec},
      {8, :lemon_juice},
      {250, :lemonade},
      :cubed_ice,
      :lemon_slices
  ]
  ```

- Maps

  ```
  %{
      id: 4,
      first_name: "Phil",
      last_name: "Collins",
      status: :immortal
  }

  %{
      keyboard: "Tony Banks",
      bass: "Mike Rutherford",
      guitar: "Mike Rutherford",
      vocals: "Phil Collins",
      drums: "Phil Collins"
  }
  ```

## Further reading

Data types

- https://elixir-lang.org/getting-started/basic-types.html
- https://elixir-lang.org/getting-started/keywords-and-maps.html
- https://elixir-lang.org/getting-started/structs.html
