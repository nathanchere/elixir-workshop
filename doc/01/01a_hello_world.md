# Hello, world!

- Start `iex` from the `src` directory

  `iex`

  ```
  Interactive Elixir (1.7.3) - press Ctrl+C to exit (type h() ENTER for help)
  iex(_)>
  ```

- Load `hello_world.ex`

  `c("hello_world.ex")`

- Call the `hello` function

  `HelloWorld.hello()`

  ```
  Hello, world!
  :ok
  ```

- Add a new function to `hello_world.ex` which accepts a name parameter

  ```
  def hello(name) do
    IO.puts("Hello, #{name}!")
  end
  ```

- Load your changes. You can either use `r HelloWorld` or `c "hello_world.ex"` if you don't want to exit and start `iex` again

- Call your new `hello` function

  `HelloWorld.hello("Phil Collins")`

  ```
  Hello, Phil Collins!
  :ok
  ```
