# Misc notes

## Function references

Functions are referenced by their name and "arity", i.e. how many arguments they take. For example:

```
def multiply(a, b) do
  a * b
end
```

would be referenced as `multiply/2` because it takes 2 arguments. If a function is part of a module and you are referencing it from outside that module, you must provide the full namespace. For example:

```
defmodule Calculator do

    def multiply(a, b) do
        a * b
    end

end
```

from outside the `Calculator` module (including from `iex`) this would be referenced as `Calculator.multiply/2`.

## Anonymous functions

You can declare anonymous functions in this format:

```
fn(arguments) -> return_value end
```

For example, to implmement the above `multiply` logic as an anonymous function you could do:

```
fn(a, b) -> a * b end
```

You can treat anonymous functions like any other variable:

```
multiply = fn(a, b) -> a * b end
```

If you want to invoke an anonymous function you just need to provide a dot after the containing variable. For example:

```
multiply = fn(a, b) -> a * b end
result = mulitply.(4,5)
```

## Passing functions to other functions

Due to the functional nature of Elixir, you will often find yourself needing to pass functions to other functions as arguments. You can use either of the above formats interchangeably. For example, the `play` function provided as an argument to `Enum/map/2` in the `fizz_buzz.ex` example is written as written as:

```
def play() do
    Enum.map(1..50, &fizzbuzz/1)
end
```

but could also be written as:

```
def play() do
    Enum.map(1..50, fn(value) -> fizzbuzz(value) end)
end
```

## Further reading

https://elixir-lang.org/getting-started/modules-and-functions.html
