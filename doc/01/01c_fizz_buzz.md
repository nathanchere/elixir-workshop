# Fizz Buzz

Open `iex` and load `fizz_buzz.ex`.

A placeholder is provided which includes a quick way of testing your work. Try it out:

`FizzBuzz.play()`

```
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
 43, 44, 45, 46, 47, 48, 49, 50]
```

This output is obviously wrong. Fix it so it outputs "Fizz", "Buzz" and "FizzBuzz" where appropriate.

## The ugliest: Labyrynth of Nested "if"s

A naive imperative approach in Elixir might look like:

```
def fizzbuzz(input) do
    if rem(input, 5) == 0 and rem(input, 3) == 0 do
        "FizzBuzz"
    else
        if rem(input, 5) == 0 do
            "Buzz"
        else
            if rem(input, 3) == 0 do
                "Fizz"
            else
                input
            end
        end
    end
end
```

## The less ugly: conditional branching

Using a `cond` statement achieves effectively the same result but easier on the eyes:

```
def fizzbuzz(input) do
  cond do
    rem(input, 15) == 0 -> "FizzBuzz"
    rem(input, 3) == 0 -> "Fizz"
    rem(input, 5) == 0 -> "Buzz"
    true -> n
  end
end
```

## The red pill: pattern matching

```
def fizzbuzz(input) when rem(input, 15) == 0, do: "FizzBuzz"
def fizzbuzz(input) when rem(input, 5) == 0, do: "Buzz"
def fizzbuzz(input) when rem(input, 3) == 0, do: "Fizz"
def fizzbuzz(input), do: input
```

## Phil Collins mode: pattern matching with single public entry point

```
def fizzbuzz(input) do
  fizzbuzz(input, rem(input, 3), rem(input, 5))
end

defp fizzbuzz(_, 0, 0), do: "FizzBuzz"
defp fizzbuzz(_, 0, _), do: "Fizz"
defp fizzbuzz(_, _, 0), do: "Buzz"
defp fizzbuzz(input, _, _), do: input
```
