# Fibonacci sequence

Open `iex` and load `fibonacci.ex`

A placeholder is provided which provides a quick way of testing a range of results, as well as a simple benchmark of your implementation's performance.

`Fibonacci.test(6)`

This output is obviously wrong. Fix it so it outputs the correct numbers in the Fibonacci sequence

## The ugliest: trying to force an imperative approach

Let's not even bother. Although there are ways to kind of do this (e.g. look up `Enum.map` or Elixir's `comprehensions` if you really want to try), it's pretty much the programming equivalent of being Adolf Bieber. Just don't. There are other languages for that.

## The basic: pattern matching and naive recursion

```
def fibonacci(0), do: 0
def fibonacci(1), do: 1
def fibonacci(input), do: fibonacci(input - 1) + fibonacci(input - 2)
```

## The red pill: tail call optimisation

```
def fibonacci(0), do: 1
def fibonacci(1), do: 1
def fibonacci(index), do: fibonacci(index, 1, 1)

defp fibonacci(2, total, previous_value), do: total

defp fibonacci(index, total, previous_value) do
  fibonacci(index - 1, total + previous_value, total)
end
```
