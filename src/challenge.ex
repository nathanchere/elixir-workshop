defmodule Challenge do
  def solve(input) do
    # TODO your solution here
    input
  end

  def solve(), do: solve(File.read!("challenge.input"))

  def test1() do
    test1("AAAbbb", "AAA")
    test1("AaabB", "AB")
    test1("abcdefghijk", "")
  end

  defp test1(input, expected) do
    IO.puts("Input: #{input}\nExpected: #{expected}\nResult: #{solve(input)}")
  end

  def test2() do
    test1("ABcD", "ABCCD")
    test1("AAAbbb", "AAABBBBBB")
    test1("AaabB", "AAAAAB")
    test1("abcdefghijk", "AABBCCDDEEFFGGHHIIJJKK")
  end

  defp test2(input, expected) do
    IO.puts("Input: #{input}\nExpected: #{expected}\nResult: #{solve(input)}")
  end
end
