defmodule Playground do
  def is_true(input) do
    input == true
  end

  def get_numbers() do
    [1, 2, 4, 8, 16, 32, 64]
  end

  def get_user(id) do
    {:ok,
     %{
       id: id,
       name: "Phil Collins",
       last_seen: ~D[2018-10-22],
       wisdom: :infinite
     }}
  end

  def get_recipe() do
    [
      {35, :jack_daniels},
      {15, :triple_sec},
      {8, :lemon_juice},
      {250, :lemonade},
      :cubed_ice,
      :lemon_slices
    ]
  end
end
