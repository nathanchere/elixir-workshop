defmodule Fibonacci do
  # def fibonacci(input) do
  #   input
  # end

  def fibonacci(0), do: 1
  def fibonacci(1), do: 1
  def fibonacci(index), do: fibonacci(index, 1, 1)

  defp fibonacci(2, total, previous_value), do: total

  defp fibonacci(index, total, previous_value) do
    fibonacci(index - 1, total + previous_value, total)
  end

  def test(input) do
    Enum.map(1..input, &fibonacci/1)
  end

  def benchmark(input) do
    start = :os.system_time(:seconds)
    result = fibonacci(input)
    finish = :os.system_time(:seconds)

    totalTime = finish - start
    IO.puts("Fibonacci sequence ##{input}: #{result}.")
    IO.puts("Your implmentation took #{totalTime} seconds to calculate the answer")
  end
end
