defmodule HelloWorld do
  def hello do
    IO.puts("Hello, world!")
  end

  def hello(name) do
    IO.puts("Hello, #{name}!")
  end
end
